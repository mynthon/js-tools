if (!window.myntools){window.myntools = {};}

window.myntools.eventies = function(firingObject){
	this.firingObject = firingObject // event will be called as method of this object
	this.registeredEvents = []
};


window.myntools.eventies .prototype = {

	_register: function(eventType, fn, always, once){
		var ed = this._processEventType(eventType)
		always = always === undefined ? true : always
		var ts = always ? 0 : +(new Date())

		if(ed.namespaces.length === 0){
			ed.namespaces.push(null)
		}

		for(var i=0; i<ed.namespaces.length; i++){
			var event = {
				'eventType': ed.type,
				'namespace': ed.namespaces[i],
				'fn': fn,
				'ts': ts,
				'once': once
			}
			this.registeredEvents.push(event)
		}
	},

	_processEventType: function(eventType){
		var x = eventType.split('.')
		var ret = {'type':'', 'namespaces':[]}
		if (x.length > 1){
			ret.type = x[0]
			ret.namespaces = x.slice(1)
		} else {
			ret.type = x[0]
		}
		return ret
	},

	/*
	 * Check if two arrays have at least one common value
	 *
	 * @param Array a1
	 * @param Array a2
	 * @returns Boolean
	 */
	_isIntersection: function(a1, a2){
		for(var i = 0; i<a1.length; i++){
			if (a2.indexOf(a1[i]) > -1){
				return true;
			}
		}
		return false;
	},

	/*
	 * @param string eventType Event type with namespace. Eg. loaded.myns
	 * @param {type} fn callback function
	 * @param {type} always If true event will be fired always, when False event
	 *						will be fired only when fire_ts will be higer
	 *						than event create time. It is important for
	 *						asynchonous js.
	 * @returns {undefined}
	 */
	register: function(eventType, fn, always){
		this._register(eventType, fn, always, false)
		return this;
	},

	/*
	 * See @register but this event is fired once only
	 */
	registerOnce: function(eventType, fn, always){
		this._register(eventType, fn, always, true)
		return this;
	},

	unregister: function(eventType){
		var et = this._processEventType(eventType)
		var ev;
		var ns = et.namespaces
		var tp = et.type


		for(var i=0; i<this.registeredEvents.length; i++){
			ev = this.registeredEvents[i]

			if (!ev){continue;}


			/* event type matches */
			if (tp === '*'){
				delete this.registeredEvents[i]
				continue
			}

			if(tp === ev.eventType) {
				if (ns.length > 0){
					if (ns[0] === '*' || ns.indexOf(ev.namespace)!==-1){
						delete this.registeredEvents[i]
						return
					}
				}else{
					delete this.registeredEvents[i]
				}
			}
		}
	},

	/*
	 * alias for fire() 
	 */
	trigger: function(eventType, ts) {
		return this.fire(eventType, ts);
	},

	/*
	 * TODO: add support for excluding ns to avoid double firing event
	 * if we want to fire eg:
	 * loaded
	 * loaded.newest
	 *
	 * @param {type} eventType
	 * @param {type} ts
	 * @returns {undefined}
	 */
	fire: function(eventType, ts){
		var et = this._processEventType(eventType)
		var ev;
		var ns = et.namespaces
		var tp = et.type
		var ts = ts || (+(new Date)) * 10

		for(var i=0; i<this.registeredEvents.length; i++){
			ev = this.registeredEvents[i]

			if (!ev){continue;}

			/* fire event with ts lower than given ns */
			if (ts < ev.ts){
				continue
			}

			/* do not fire event if types doesn't match */
			if (tp !== '*' && tp !== ev.eventType) {
				continue
			}

			/*
			 * fired has namespaces and first name space isnt equal to '*'
			 * currently checked event isnt in called events
			 */
			if (ns.length > 0){
				if (ns[0] !== '*' && ns.indexOf(ev.namespace)===-1){
					continue
				}
			}

			ev.fn.call(this.firingObject)

			if (ev.once){
				delete this.registeredEvents[i]
			}
		}
	}
}
