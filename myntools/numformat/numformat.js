if (!window.myntools){window.myntools = {};}


/*
 * format options
 * +# show rounded number with plus sign (10.998 => +10)
 * +#.## show number with plus sign and maximum two decimal places (10.998 => +10.1)
 * #.00 show number with two decimal places (10.998 => 10.10)
 * #. show number with all decimal places (10.998 => 10.998)
 *
 *
 */

window.myntools.numFormat = {

	Formatter: function(format, options){
		var lib = window.myntools.numFormat;
		var o = options || {}
		this._format = format;
		this._decimal = o.decimal || '.';
		this._separator = o.separator || '';
		this._sign = '+';
		this._parsedFormat = lib.parseFormat(this._format);


		// METHODS

		this.setLocale = function(locale){
		//todo
		},

		/*
		 * formats given number to string output
		 */
		this.format = function(numberToFormat){
			var numStr = numberToFormat.toString();
			var numSplitted = numStr.split('.')
			var base = numSplitted[0]
			var decimals = numSplitted[1] || ''

			var formatDetails = this._parsedFormat;
			var showSign = formatDetails.sign && numberToFormat > 0
			var baseLen = formatDetails.baseLen;
			var decLen = formatDetails.decLen;
			var decType = formatDetails.decType;

			decimals = lib.formatDecimals(decimals, decLen, decType);

			var textnum = base;
			if (this._separator !== '') {
				var re = /(\d+)(\d{3})/;
				while (re.test(textnum)) {
					textnum = textnum.replace(re, '$1' + this._separator + '$2')
				}
			}

			if (decimals.length){
			  textnum = textnum + this._decimal + decimals;
			}

			if(showSign){
			  textnum = this._sign + textnum;
			}

			if (formatDetails.fillLen > 0){
				textnum = lib.getFilled(
					textnum,
					formatDetails.fillLen,
					formatDetails.filler,
					formatDetails.fillType
				);
			}

			return textnum;
		};

		/*
		 * parses given string to number
		 */
		this.parse = function (strNumber) {
			var separator = this._separator;
			var decimal = this._decimal;

			var matchRe = new RegExp('-?([0-9]+\\' + separator + '?)+(\\' + decimal + '[0-9]+)?')
			var separatorRe = new RegExp('\\' + separator, 'g');

			var extracted = strNumber.match(matchRe)[0].replace(separatorRe, '');
			if (extracted) {
				if (extracted.indexOf(decimal) > 0) {
					return parseFloat(extracted.replace(decimal, '.'));
				} else {
					return parseInt(extracted);
				}
			}
			return null;
		}
	},

	getFilled: function (strNumber, expectedLength, filler, fillType) {
		var out = strNumber;
		var diff = expectedLength - ('' + strNumber).length;
		if (diff > 0) {
			if (fillType === '>') {
				out = Array(diff + 1).join(filler) + strNumber;
			} else if (fillType === '<') {
				out = strNumber + Array(diff + 1).join(filler);
			} else {
				var l = Math.ceil(diff / 2);
				var r = Math.floor(diff / 2);
				out = Array(l + 1).join(filler) + strNumber + Array(r + 1).join(filler);
			}
		}
		return out;
	},

	parseFormat: function (format) {
		var fillLen = 0;
		var fillType = '=';
		var filler = '';

		var hasFiller = format.match(/^(<|=|>)([0-9]+)(.)(.*)/);
		if (hasFiller){
			fillType = hasFiller[1];
			fillLen = parseInt(hasFiller[2]);
			filler = hasFiller[3];
			format = hasFiller[4];
		}

		var sign = /\+/.test(format);
		var temp = format.replace(/[^.#0]/g, '');
		temp = temp.split('.');
		var decLength;
		var decimalPart = '';

		if (temp.length === 2) {
			if (temp[1].length > 0) {
				decLength = temp[1].length;
				decimalPart = temp[1]
			} else {
				decLength = -1;
				decimalPart = temp[1]
			}
		} else {
			decLength = 0;
		}

		return {
			'sign': sign,
			'basePart': temp[0],
			'decPart': decimalPart,
			'decLen': decLength,
			'baseLen': temp[0].length,
			'decType': decimalPart[0] === '0' ? '0' : '#',
			'fillLen': fillLen,       //todo: fill with @filler to this length
			'fillType': fillType,     //todo: fill side
			'filler': filler          //todo: fill sign
		};
	},

	formatDecimals: function(decimalsStr, expectedLength, type){
		var finalDec = ''

		if (expectedLength === 0){
			//return '';
		}else if(expectedLength === -1){
			finalDec = decimalsStr;
		} else {
			var tdnum = parseFloat('0.' + decimalsStr);
			var pow = Math.pow(10, expectedLength);
			finalDec = (Math.round(tdnum*pow)/pow).toString().replace(/0\./, '');
		}

		if (type === '0' && finalDec.length < expectedLength) {
			return finalDec + Array(expectedLength - finalDec.length + 1).join('0');
		} else {
			if (/^0+$/.test(finalDec)){
				return '';
			}else{
				return finalDec;
			}
		}
	}
}











