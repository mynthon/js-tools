if (!window.myntools){window.myntools = {};}

window.myntools.DataValid = function(){
	this._isValid = false;
	this.messages = []
	this.validators = window.myntools.DataValid.getBuiltinValidators ? window.myntools.DataValid.getBuiltinValidators() : {}

	/**
	 * validatorConfig.message		Default message when error. Message can use special variables: {%fieldname%} for
	 * 								displaying field name and {%value[i]%} is a value of param passed to validating
	 * 								function, where [i] should be replaced with param index. Eg. if validator takes 3
	 * 								arguments, you can access them by using {%value0%}, {%value1%} and {%value2%}.
	 * validatorConfig.validator	Function that validates data. Must return true/false.
	 *
	 * @param {type} validatorConfig
	 * @returns DataValid Validator instance
	 */
	this.addValidator = function(validatorName, validatorConfig) {
		this.validators[validatorName] = validatorConfig;
		return this;
	}

	/**
	 * @param Object dataToValidate		List of objects with validation params (later referenced as DTV).
	 * 			dataToValidate is object in format
	 * 			{
	 * 				'fieldName1:{'param1':value1, 'param2':value2},
	 * 				'fieldName1:{'param1':value1, 'param2':value2}
	 * 			}
	 * 			Available params are described below.
	 * @param Object	DTV.fieldParam.validator	Name of validator to use
	 * @param Object	DTV.fieldParam.success		Callback called when data is valid
	 * @param Object	DTV.fieldParam.error		Callback called when data is not valid
	 * @param Array		DTV.fieldParam.data			Array of arguments passed to validating function.
	 *												In most cases first element is data to validate.
	 * @param Object	DTV.fieldParam.message		Replaces validators original message.
	 */
	this.validate = function(dataToValidate) {
        this._isValid = true;
        this.messages = [];

		for(var fieldname in dataToValidate) {
			var current = dataToValidate[fieldname];
			if (current.validator in this.validators) {
				var result = this.validators[current.validator].validator.apply(this, current.data);

				if (result) {
					if (current.success) { current.success(current); }
				} else {
					this._isValid = false;
					var msg = current.message ? current.message : this.validators[current.validator].message
					msg = msg.replace('{%fieldname%}', fieldname);
					for(var i = 0; i < current.data.length; i++) {
						msg = msg.replace('{%value' + i + '%}', current.data[i]);
					}
					this.messages.push(msg)
					if (current.error) { current.error(current); }
				}
			} else {
				this._isValid = false;
				this.messages.push(['-', 'Validator ' + current.validator + ' not exists.']);
			}
		}
	}

	/**
	 * Returns true if all elements passed last validation
	 *
	 * @returns {Boolean}
	 */
	this.isValid = function() {
		return this._isValid;
	}

	/**
	 * Returns array with all messages for last validation
	 *
	 * @returns {Array}
	 */
	this.getMessages = function(){
		return this.messages;
	}
}


/**
 * Built in validators
 */
window.myntools.DataValid.getBuiltinValidators = function()  {
	return {

		/**
		 * Returns true for values
		 */
		is_true: {
			validator: function(data){
				var expr = [true, 1, 'true', '1', 'on', 'yes', 'ok'];
				if (typeof(data) === 'string'){data = data.toLowerCase();}
				return (expr.indexOf(data) > -1);
			},
			message: '{%fieldname%} can\'t be accepted as true.'
		},

		/**
		 * Validator checks if object is string and is not empty.
		 */
		not_empty_string: {
			validator: function(data){ return ((typeof data === 'string' && data !== "")); },
			message: '{%fieldname%} is empty string.'
		},

		/**
		 * Validator checks if type and value of both objects are equal.
		 */
		equals: {
			validator: function(data, compareValue){ return ((data === compareValue)); },
			message: '{%fieldname%} is not equal to : {%value1%}.'
		},

		/**
		 * Validator checks if object is not undefined, null or empty string
		 */
		required: {
			validator: function(data){
				if (data === undefined || data === null) { return false; }
				if (typeof data === 'string' && data === '') { return false; }
				return true;
			},
			message: '{%fieldname%} is required.'
		},

		/**
		 * Validator checks if object is number or string in decimal format (eg. 124.78)
		 */
		numberic: {
			validator: function(data){
				if (typeof data === 'number') {	return true; }
				if (typeof data === 'string' && /^(\+|-)?[0-9]+(.[0-9]+)?$/.test(data)) { return true; }
				return false;
			},
			message: '{%fieldname%} is not safe numeric.'
		},

		/**
		 * Validator check if int is between min and max (included) or string/array length
		 * is between min and max (included)
		 */
		size_between: {
			validator: function(data, min, max) {
				if (typeof data === 'number' && data >= min && data <= max) { return true}
				if (typeof data === 'string' && data.length >= min && data.length <= max) { return true; }
				if (Object.prototype.toString.call(data) === '[object Array]' && data.length >= min && data.length <= max){ return true; }
				return false;
			},
			message: '{%fieldname%} size is not between {%value1%} and {%value2%}.'
		},

		/**
		 * Advanced email check.
		 */
		email: {
			validator: function(data) {
				// https://stackoverflow.com/a/16016476/269301
				var sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
				var sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
				var sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
				var sQuotedPair = '\\x5c[\\x00-\\x7f]';
				var sDomainLiteral = '\\x5b(' + sDtext + '|' + sQuotedPair + ')*\\x5d';
				var sQuotedString = '\\x22(' + sQtext + '|' + sQuotedPair + ')*\\x22';
				var sDomain_ref = sAtom;
				var sSubDomain = '(' + sDomain_ref + '|' + sDomainLiteral + ')';
				var sWord = '(' + sAtom + '|' + sQuotedString + ')';
				var sDomain = sSubDomain + '(\\x2e' + sSubDomain + ')*';
				var sLocalPart = sWord + '(\\x2e' + sWord + ')*';
				var sAddrSpec = sLocalPart + '\\x40' + sDomain; // complete RFC822 email address spec
				var sValidEmail = '^' + sAddrSpec + '$'; // as whole string
				return (new RegExp(sValidEmail)).test(data.toLowerCase())
			},
			message: '{%fieldname%} is not email.'
		},

		/**
		 * Very simple and fast method of checking if element is email.
		 */
		email_simple: {
			validator: function(data) {
				return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(data);
			},
			message: '{%fieldname%} is not email.'
		},

		/**
		 * Validates string against custom regexp
		 */
		regexp: {
			validator: function(data, regexp, flags) {
				var re;
				if ( Object.prototype.toString.call(regexp) === "[object RegExp]") {
					re = regexp;
				} else if (typeof regexp === 'string') {
					var flags = (typeof flags === 'string') ? flags : '';
					re = new RegExp(regexp, flags);
				} else {
					return false;
				}

				return re.test(data)
			},
			message: '{%fieldname%} doesn\'t match {%value1%}.'
		}
	}
} 
